TEX = xelatex -halt-on-error

OUTDIR = dist

all: ${OUTDIR}/statut.pdf

${OUTDIR}:
	mkdir -p ${OUTDIR}

${OUTDIR}/%.pdf: %.tex | ${OUTDIR}
	$(TEX) -output-directory=${OUTDIR} $<
	$(TEX) -output-directory=${OUTDIR} $<

clean:
	-rm -r ${OUTDIR}

.PHONY: all clean
.SUFFIXES: .pdf .tex
